export GUILE_LOAD_PATH := $(GUILE_LOAD_PATH):./:$(XDG_CONFIG_HOME)/guix/current/share/guile/site/3.0

.PHONY: kazbek-system
kazbek-system:
	guix system image -t iso9660 ./system/kazbek.scm
